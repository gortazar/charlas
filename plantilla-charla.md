# Título de la propuesta

Pequeña introducción y motivación de la misma.

## Formato de la propuesta

Indicar uno de estos:

* [ ] Charla (25 minutos)
* [ ] Charla relámpago (10 minutos)

## Descripción

Descripción de un par de párrafos sobre de qué va la charla.

## Público objetivo

¿A quién va dirigida? 

## Ponente(s)

¿Quién o quienes van a dar la charla? ¿Qué hacen? ¿Qué charlas han
dado antes?

## Condiciones

* [ ] Acepto seguir el [código de conducta](https://eslib.re/2019/conducta/) y solicitar a los asistentes y ponentes esta aceptación.
* [ ] Al menos una persona entre los que la proponen estará presente el día programado para la charla.


